package ru.myitschool.mte;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.constraintlayout.utils.widget.ImageFilterButton;

import ru.myitschool.mte.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    ImageFilterButton imageFilterButton;

    int[] cars = {R.drawable.car1, R.drawable.car2, R.drawable.car3};
    int carNow = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        imageFilterButton = findViewById(R.id.ib_car);

        imageFilterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                carNow += 1;
                carNow %= 3;
                Drawable drawable = AppCompatResources.getDrawable(getApplicationContext(), cars[carNow]);
                imageFilterButton.setImageDrawable(drawable);
            }
        });
    }
}
